// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "MayhemGameMode.generated.h"

UCLASS(minimalapi)
class AMayhemGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AMayhemGameMode();
};



