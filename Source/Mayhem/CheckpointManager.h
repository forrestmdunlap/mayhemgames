// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "CheckpointManager.generated.h"

UCLASS()
class MAYHEM_API ACheckpointManager : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACheckpointManager();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	//Sets index of current checkpoint
	void setCheckpoint(class ACheckpointTriggerBox* cp);
	FVector getCurrentSpawnLocation();

private:

	UPROPERTY(EditAnywhere)
		class ACheckpointTriggerBox* CurrentCheckPoint;
	
};
